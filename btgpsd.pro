TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle

QT -= qml gui
QT += bluetooth positioning

SOURCES += main.cpp \
    bluetoothgpsserver.cpp \
    nmeasource.cpp

HEADERS += \
    bluetoothgpsserver.h \
    nmeasource.h

OTHER_FILES += \
    rpm/btgpsd.yaml rpm/btgpsd.spec \
    btgpsd.service

target.path = /usr/sbin
INSTALLS += target

unit.path = /usr/lib/systemd/user/
unit.files = btgpsd.service
INSTALLS += unit
