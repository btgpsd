#ifndef NMEASOURCE_H
#define NMEASOURCE_H

#include <QtCore/QObject>
#include <QtCore/QString>

#include <QtPositioning/QGeoPositionInfo>
#include <QtPositioning/QGeoPositionInfoSource>
#include <QtPositioning/QGeoSatelliteInfoSource>

class NmeaSource : public QObject
{
	Q_OBJECT
public:
	explicit NmeaSource(QObject *parent = 0);
	~NmeaSource();

signals:
	void dataReady(const QString& data);

public slots:
	void start();
	void stop();

protected:
	static const double H_UERE = 15.0;
	static const double V_UERE = 23.0;
	static const double P_UERE = 19.0;

	enum PositionAccuracyDirection {
		Spherical,
		Horizontal,
		Vertical
	};

	static QString formatFixMode(const QGeoCoordinate& coord);
	static QString formatTimestamp(const QTime& time);
	static QString formatDatestamp(const QDate& date);
	static QString formatLatitude(const QGeoCoordinate& coord);
	static QString formatLatitudeNS(const QGeoCoordinate& coord);
	static QString formatLongitude(const QGeoCoordinate& coord);
	static QString formatLongitudeEW(const QGeoCoordinate& coord);
	static QString formatAltitude(const QGeoCoordinate& coord);
	static QString formatAltitudeUnits(const QGeoCoordinate& coord);
	static QString formatSpeedKnots(const QGeoPositionInfo& pos);
	static QString formatSpeedKmH(const QGeoPositionInfo& pos);
	static QString formatTrueCourse(const QGeoPositionInfo& pos);
	static QString formatMagCourse(const QGeoPositionInfo& pos);
	static QString formatMagVariation(const QGeoPositionInfo& pos);
	static QString formatMagVariationEW(const QGeoPositionInfo& pos);
	static QString formatHDOP(const QGeoPositionInfo& pos);
	static QString formatVDOP(const QGeoPositionInfo& pos);
	static QString formatPDOP(const QGeoPositionInfo& pos);
	static QString formatError(const QGeoPositionInfo& pos,
							   PositionAccuracyDirection which = Spherical);
	static QString formatErrorUnits(const QGeoPositionInfo& pos,
									PositionAccuracyDirection which = Spherical);
	static QString formatSatPrn(const QGeoSatelliteInfo& sat);
	static QString formatSatElev(const QGeoSatelliteInfo& sat);
	static QString formatSatAz(const QGeoSatelliteInfo& sat);
	static QString formatSatSnr(const QGeoSatelliteInfo& sat);

	void emitSentence(const QString& sentence);

	void generateGPRMC(const QGeoPositionInfo& pos);
	void generateGPGGA(const QGeoPositionInfo& pos);
	void generateGPGLL(const QGeoPositionInfo& pos);
	void generateGPVTG(const QGeoPositionInfo& pos);
	void generatePGRME(const QGeoPositionInfo& pos);

	void generateGPGSA(const QGeoPositionInfo& pos, const QList<QGeoSatelliteInfo>& info);
	void generateGPGSV(const QGeoPositionInfo& pos, const QList<QGeoSatelliteInfo>& info);

protected slots:
	void handlePosition(const QGeoPositionInfo& pos);
	void handleSatsInUse(const QList<QGeoSatelliteInfo>& info);
	void handleSatsInView(const QList<QGeoSatelliteInfo>& info);

private:
	QGeoPositionInfoSource *m_possrc;
	QGeoSatelliteInfoSource *m_satsrc;
	QGeoPositionInfo m_last_pos;
	int m_last_satsinview;
};

#endif // NMEASOURCE_H
