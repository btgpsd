#include <QtCore/QCoreApplication>

#include "bluetoothgpsserver.h"

int main(int argc, char *argv[])
{
	QCoreApplication app(argc, argv);
	QScopedPointer<BluetoothGpsServer> server(new BluetoothGpsServer(0));

	server->start();

	return app.exec();
}
