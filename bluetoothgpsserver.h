#ifndef BLUETOOTHGPSSERVER_H
#define BLUETOOTHGPSSERVER_H

#include <QtCore/QObject>
#include <QtCore/QList>

#include <QtBluetooth/QBluetoothSocket>
#include <QtBluetooth/QBluetoothServiceInfo>
#include <QtBluetooth/QBluetoothServer>

class NmeaSource;

class BluetoothGpsServer : public QObject
{
	Q_OBJECT
public:
	explicit BluetoothGpsServer(uint m_port, QObject *parent = 0);
	~BluetoothGpsServer();

signals:

public slots:
	void start();
	void stop();

protected slots:
	void sendData(const QString& data);
	void acceptConnection();
	void handleDisconnection();

private:
	uint m_port;
	NmeaSource* m_source;
	QBluetoothServiceInfo m_service;
	QBluetoothServer* m_server;
	QList<QBluetoothSocket*> m_clients;
};

#endif // BLUETOOTHGPSSERVER_H
